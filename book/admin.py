from django.contrib import admin
from .models import Card, PhoneNumber

admin.site.register(Card)
admin.site.register(PhoneNumber)
