from django.http import Http404
from rest_framework import generics, status, filters
from rest_framework.pagination import PageNumberPagination
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.shortcuts import HttpResponse
from django.contrib.auth.models import User
from ..models import Card, PhoneNumber
from .serializers import CardSerializer, PhoneSerializer


class CustomPagePagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 7


@method_decorator(login_required, name='dispatch')
class CardListView(generics.ListCreateAPIView):
    # queryset = Card.objects.all().order_by('surname')
    serializer_class = CardSerializer
    pagination_class = CustomPagePagination
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name','surname')

    def post(self, *args, **kwargs):
        res = super().post(*args, **kwargs)
        if res.status_code == status.HTTP_201_CREATED:
            pass
        return res

    def get_queryset(self, *args, **kwargs):
        return Card.objects.all().filter(user=self.request.user).order_by('surname')


@method_decorator(login_required, name='dispatch')
class CardView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Card.objects.all()
    serializer_class = CardSerializer

    def post(self, *args, **kwargs):
        res = super().post(*args, **kwargs)
        if res.status_code == status.HTTP_201_CREATED:
            pass
        return res

    def get_object(self):
        try:
            card = get_object_or_404(Card, id=self.kwargs.get('card_id'), user=self.request.user)
            return card
        except Exception as e:
            raise Http404


def HomeView(request):
    return render(request, 'home.html')

def BookView(request):
    return render(request, 'main_book.html')