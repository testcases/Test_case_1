from django.db import models
from django.contrib.auth.models import User

class PhoneNumber(models.Model):

    number = models.CharField(max_length = 32, unique=True, null=True, blank=True)

    def __str__(self):
        return "Ph: %s" % (self.number)

    @property
    def related_cards(self):
        # res = self.card.all().exclude(id=self.id) #
        res = self.card.all()

        return res

class Card(models.Model):

    SUPPORTED_UNITS = (
        ('Autonomous Republic of Crimea','Autonomous Republic of Crimea'),
        ('Cherkasy oblast','Cherkasy oblast'),
        ('Chernihiv oblast','Chernihiv oblast'),
        ('Chernivtsi oblast','Chernivtsi oblast'),
        ('Dnipro oblast','Dnipro oblast'),
        ('Donetsk oblast','Donetsk oblast'),
        ('Ivano-Frankivsk oblast','Ivano-Frankivsk oblast'),
        ('Kharkiv oblast','Kharkiv oblast'),
        ('Kherson oblast','Kherson oblast'),
        ('Khmelnytskyi oblast','Khmelnytskyi oblast'),
        ('Kirovohrad oblast','Kirovohrad oblast'),
        ('Kyiv oblast','Kyiv oblast'),
        ('Luhansk oblast','Luhansk oblast'),
        ('Lviv oblast','Lviv oblast'),
        ('Mykolaiv oblast','Mykolaiv oblast'),
        ('Odesa oblast','Odesa oblast'),
        ('Poltava oblast','Poltava oblast'),
        ('Rivne oblast','Rivne oblast'),
        ('Sum oblast','Sumy oblast'),
        ('Ternopil oblast','Ternopil oblast'),
        ('Vinnytsia oblast','Vinnytsia oblast'),
        ('Volyn oblast','Volyn oblast'),
        ('Zakarpattia oblast','Zakarpattia oblast'),
        ('Zaporizhzhia oblast','Zaporizhzhia oblast'),
        ('Zhytomyr oblast','Zhytomyr oblast'),
    )

    name = models.CharField(max_length = 100)
    surname = models.CharField(max_length = 100)
    location = models.CharField(max_length = 30, choices=SUPPORTED_UNITS, null=True)
    email = models.EmailField(max_length = 100,unique=True, )
    phone = models.ManyToManyField(PhoneNumber,  related_name='card')# on_delete=models.SET_NULL,
    user = models.ForeignKey(User,related_name='card')

    def __str__(self):
            return "%s, %s" % (self.surname, self.name[0])
