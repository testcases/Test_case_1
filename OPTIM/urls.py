from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from book.api.views import HomeView
from book.signals import create_card

urlpatterns = [
    url(r'^$', HomeView, name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('book.api.urls')),

    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'logout.html'}, name='logout'),

]
